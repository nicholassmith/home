--vim.opt.termguicolors = true
vim.opt.background = "light"
vim.opt.ignorecase = true
vim.opt.laststatus = 2
vim.opt.ruler = true
vim.opt.number = true
--vim.opt.relativenumber = true
vim.opt.shiftwidth = 2
vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.expandtab = true
vim.opt.smartindent = true
vim.opt.incsearch = true
vim.opt.backspace = indent,eol,start
vim.opt.scroll = 20
vim.opt.statusline = " %{fnamemodify(getcwd(),':t')} [%{g:current_git_branch}] %f +%l %c %o %m%=%{$DOCKER} %{g:current_file_time} %P %LL %y %r"
vim.opt.hlsearch = false
vim.opt.cinoptions = "=0" -- set case block indentation inline with case
vim.opt.autoread = true -- reload changed files on disk
vim.opt.lazyredraw = true
vim.opt.undofile = true -- remember file undo history
vim.opt.grepprg = "rg --vimgrep"
vim.opt.splitbelow = true
vim.opt.exrc = true
vim.opt.secure = true
vim.opt.guicursor = ""
vim.opt.mouse = ""
vim.opt.directory = "."
