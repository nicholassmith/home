local Plug = vim.fn['plug#']

vim.call('plug#begin', '~/.config/nvim/plugged')

Plug('junegunn/fzf', { ["do"] = vim.fn["fzf#install()"] })
Plug 'junegunn/fzf.vim'
Plug('psf/black', { branch = 'stable' })
Plug 'tikhomirov/vim-glsl'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'rust-lang/rust.vim'
Plug 'sdiehl/vim-ormolu'
Plug 'github/copilot.vim'
Plug 'terrastruct/d2-vim'
Plug('mrcjkb/haskell-tools.nvim', { version = '^3', ft = { 'haskell', 'lhaskell', 'cabal', 'cabalproject' } })
Plug("nvim-treesitter/nvim-treesitter", { ["do"] = ":TSUpdate" })
-- LSP
Plug("williamboman/mason.nvim", { ["do"] = ":MasonUpdate" })
Plug 'williamboman/mason-lspconfig.nvim'
Plug("neovim/nvim-lspconfig")
Plug("hrsh7th/nvim-cmp")
Plug("hrsh7th/cmp-nvim-lsp")
Plug("nicholashaydensmith/f.nvim")

vim.call('plug#end')

vim.g.ormolu_options={"--no-cabal"}
