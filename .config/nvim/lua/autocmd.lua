local autocmd = vim.api.nvim_create_autocmd

autocmd({"BufEnter", "BufRead", "BufWrite"}, {
  pattern = {"*"},
  command = "FileTime",
})
autocmd({"BufEnter"}, {
  pattern = {"*"},
  command = "CurrentGitBranch",
})
autocmd({"TermOpen"}, {
  pattern = {"*"},
  command = "silent execute 'normal! G'"
})
autocmd({"FileType"}, {
  pattern = {"make"},
  command = "setlocal noexpandtab",
})
autocmd({"FileType"}, {
  pattern = {"/etc/sudoers.tmp"},
  command = "setlocal noexpandtab",
})
autocmd({"FileType"}, {
  pattern = {"javascript"},
  command = "setlocal shiftwidth=2 softtabstop=2 tabstop=4 expandtab",
})
autocmd({"FileType"}, {
  pattern = {"python"},
  command = "setlocal shiftwidth=4 softtabstop=4 tabstop=8 expandtab",
})
--autocmd({"FileType"}, {
--  pattern = {"python"},
--  command = "nnoremap <silent> K :<C-u>execute 'silent !python3 -c "help(\"' . expand("<cword>") . '\")"'<CR><C-l>",
--})
--autocmd({"FileType"}, {
--  pattern = {"python"},
--  command = "vnoremap <silent> K y:<C-u>execute 'silent !python3 -c "help(' . shellescape(@", 1) . ')"'<CR><C-l>",
--})
autocmd({"FileType"}, {
  pattern = {"python"},
  command = "nnoremap <silent> <leader>i :silent botright split \\| botright terminal python3 -i -c \"from importlib.machinery import SourceFileLoader as _s; _m = _s('_m', '%').load_module(); from _m import *;\"<cr>",
  
})
autocmd({"TermOpen"}, {
  pattern = {"*"},
  command = "setlocal nonumber norelativenumber",
})
autocmd({"TermOpen"}, {
  pattern = {"term://*python3*-i*"},
  command = "startinsert",
})
autocmd({"TermOpen"}, {
  pattern = {"term://*fzf*"},
  command = "startinsert",
})
autocmd({"TermClose"}, {
  pattern = {"term://*python3*-i*"},
  command = "lua vim.api.nvim_input(\"<CR>\")",
})
autocmd({"FileType"}, {
  pattern = {"idris2"},
  command = "nnoremap <silent> <leader><CR> :silent term ++close idris2 %<CR>",
})
autocmd({"FileType"}, {
  pattern = {"haskell"},
  command = "setlocal expandtab shiftwidth=2 softtabstop=2 tabstop=2",
})
--autocmd({"FileType"}, {
--  pattern = {"haskell"},
--  command = "nnoremap <silent> K :silent !stack hoogle "<cword>" -- --info<CR>:redraw!<CR>",
--})
autocmd({"FileType"}, {
  pattern = {"haskell"},
  command = "nnoremap <silent> <leader><CR> :silent term ++close stack ghci --ghci-options -XOverloadedStrings %<CR>",
})
autocmd({"FileType"}, {
  pattern = {"cabal"},
  command = "setlocal expandtab",
})
autocmd({"Filetype"}, {
  pattern = {"yaml"},
  command = "setlocal expandtab shiftwidth=2 softtabstop=2 tabstop=2",
})
autocmd({"Filetype"}, {
  pattern = {"markdown"},
  command = "setlocal expandtab shiftwidth=2 softtabstop=2 tabstop=2 foldmethod=indent nofoldenable textwidth=80",
})
--autocmd({"Filetype"}, {
--  pattern = {"markdown"},
--  command = "nnoremap <silent> <leader>j :.s/\[\([x ]\)\]/\=submatch(1) == ' ' ? '[x]' : '[ ]'/g<CR>:noh<CR>:w<CR>",
--})
autocmd({"BufRead", "BufNewFile"}, {
  pattern = {"*.mm"},
  command = "setlocal filetype=objcpp",
})
autocmd({"BufRead", "BufNewFile"}, {
  pattern = {"*.nix"},
  command = "setlocal shiftwidth=2 softtabstop=2 tabstop=2 expandtab filetype=nix",
})
autocmd({"BufRead", "BufNewFile"}, {
  pattern = {"*.tpp"},
  command = "setlocal filetype=cpp",
})
autocmd({"BufRead", "BufNewFile"}, {
  pattern = {"*.inl"},
  command = "setlocal filetype=cpp",
})
autocmd({"BufRead", "BufNewFile"}, {
  pattern = {"*.ini"},
  command = "setlocal filetype=cpp",
})
autocmd({"BufRead", "BufNewFile"}, {
  pattern = {"*.inc"},
  command = "setlocal filetype=cpp",
})
autocmd({"BufRead", "BufNewFile"}, {
  pattern = {"*.metal"},
  command = "setlocal filetype=cpp",
})
autocmd({"BufRead", "BufNewFile"}, {
  pattern = {"*.cl"},
  command = "setlocal filetype=cpp",
})
autocmd({"BufRead", "BufNewFile"}, {
  pattern = {"*.hlsl"},
  command = "setlocal filetype=cpp",
})
autocmd({"BufRead", "BufNewFile"}, {
  pattern = {"*.ll"},
  command = "set filetype=llvm",
})
autocmd({"BufRead", "BufNewFile"}, {
  pattern = {"*.llvm"},
  command = "set filetype=llvm",
})
autocmd({"BufRead", "BufNewFile"}, {
  pattern = {"*.mlir"},
  command = "set filetype=mlir",
})
autocmd({"BufRead", "BufNewFile"}, {
  pattern = {"*.td"},
  command = "set filetype=tablegen",
})
autocmd({"BufRead", "BufNewFile"}, {
  pattern = {"*.fbs"},
  command = "set filetype=fbs",
})
autocmd({"BufRead", "BufNewFile"}, {
  pattern = {"*.jsonpp"},
  command = "set filetype=json",
})
autocmd({"BufRead", "BufNewFile"}, {
  pattern = {"*.gltf"},
  command = "set filetype=json",
})
autocmd({"BufWritePre"}, {
  pattern = {"*.h","*.hpp","*.c","*.cpp","*.cc"},
  command = "py3f ~/.clang-format.py"
})
