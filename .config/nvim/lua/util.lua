vim.api.nvim_exec([[
function! s:FileTime()
	let g:current_file_time = strftime("%H:%M:%S %d-%b-%y", getftime(expand("%")))
	return g:current_file_time
endfunction
command! FileTime call s:FileTime()

function! s:CurrentGitBranch()
	let g:current_git_branch = system('(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || git symbolic-ref --short HEAD 2>/dev/null || git rev-parse --short HEAD 2>/dev/null) | tr -d "\n"')
	return g:current_git_branch
endfunction
command! CurrentGitBranch call s:CurrentGitBranch()

command! Gres Ggrep "<<<<<<< HEAD"<cr>

command! Gcheckout terminal git for-each-ref --sort=-committerdate --format='\%(refname:short)' refs/heads/ | FZF_DEFAULT_OPTS="--height ${FZF_TMUX_HEIGHT:-40\%} --reverse $FZF_DEFAULT_OPTS $FZF_COMPLETION_OPTS" fzf | xargs git checkout
]], false)
