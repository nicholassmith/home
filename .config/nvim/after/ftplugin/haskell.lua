local ht = require('haskell-tools')
local opts = { noremap = true, silent = true, buffer = true, }

-- haskell-language-server relies heavily on codeLenses,
-- so auto-refresh (see advanced configuration) is enabled by default
-- vim.keymap.set('n', '<space>ca', vim.lsp.codelens.run, opts)
-- Hoogle search for the type signature of the definition under the cursor

vim.api.nvim_create_autocmd({"FileType"}, {
  pattern = {"haskell"},
  callback = function()
    vim.keymap.set({'n', 'v'}, 'K', ht.hoogle.hoogle_signature, opts)
  end
})

-- Evaluate all code snippets
--vim.keymap.set('n', '<space>ea', ht.lsp.buf_eval_all, opts)
-- Toggle a GHCi repl for the current package
--vim.keymap.set('n', '<leader>i', ht.repl.toggle, opts)
-- Toggle a GHCi repl for the current buffer
vim.keymap.set('n', '<leader>i', function() ht.repl.toggle(vim.api.nvim_buf_get_name(0)) end, def_opts)
vim.keymap.set('n', '<leader>q', ht.repl.quit, opts)
