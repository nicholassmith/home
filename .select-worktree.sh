EDITOR=~/.local/bin/nvim
session=$(basename $(pwd))
branch=$(tmux -L worktree list-windows -F "#W" | ~/.config/nvim/plugged/fzf/bin/fzf --reverse)
win=$(tmux -L worktree list-windows -F "#I #W" | grep "$branch" | awk '{print $1;}')
tmux -L worktree select-window -t "$win"
