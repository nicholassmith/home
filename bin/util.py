from __future__ import print_function
import urllib
import sys
import os
import json
import re
import multiprocessing
import subprocess
import inspect
import hashlib
import pickle
import functools
import http.server
from copy import copy
from pprint import pprint

DEVNULL = open(os.devnull, 'w')

cache_timeout=60*60*2 # 2 Hours
cache_dir = '/tmp/pyutil.cache'

def cached(f):
    default_cached_kwargs = {
            '_cached': True,
            '_cache_invalidate': False,
            '_retry_attempts': 5,
            }

    def extract_cached_args(kwargs):
        to_dict = lambda s: dict(((k, kwargs[k]) for k in s))
        keys = set(kwargs)
        default_keys = set(default_cached_kwargs)
        specified_cached_kwargs = to_dict(keys & default_keys)
        f_kwargs = to_dict(keys - default_keys)
        cached_kwargs = copy(default_cached_kwargs)
        cached_kwargs.update(specified_cached_kwargs)
        return cached_kwargs, f_kwargs

    def cache_invalidate(file_path):
        try:
            os.remove(file_path)
        except OSError: pass

    @functools.wraps(f)
    def cached_wrapper(*args, **kwargs):
        cached_kwargs, f_kwargs = extract_cached_args(kwargs)
        bind_args = inspect.getcallargs(f, *args, **f_kwargs)
        contents = None
        file_name = hashlib.sha1(str((f.__name__,) + tuple(bind_args.items())).encode('utf-8')).hexdigest()
        if not os.path.exists(cache_dir):
            os.makedirs(cache_dir)
        file_path = '/'.join([cache_dir, file_name])

        def retry_wrapper():
            for attempt in range(0, cached_kwargs['_retry_attempts']-1):
                try:
                    return f(*args, **f_kwargs)
                except:
                    cache_invalidate(file_path)
            try:
                return f(*args, **f_kwargs)
            finally:
                cache_invalidate(file_path)

        if cached_kwargs['_cache_invalidate']:
            cache_invalidate(file_path)

        if cached_kwargs['_cached']:
            try:
                with open(file_path, 'rb') as fd:
                    contents = pickle.loads(fd.read())
            except:
                contents = retry_wrapper()
                if not hasattr(contents, 'read'):
                    with os.fdopen(os.open(file_path, os.O_WRONLY | os.O_CREAT | os.O_EXLOCK), 'wb') as fd:
                        fd.write(pickle.dumps(contents))
        else:
            contents = retry_wrapper()

        return contents

    return cached_wrapper

def clear_cache():
    subprocess.call(['rm', '-r', cache_dir])

class ProcessSingleton:
    def __init__(self, decorated):
        self._decorated = decorated
        self._instance = {}

    def Instance(self, *args, **kwargs):
        pid = os.getpid()
        try:
            return self._instance[pid]
        except KeyError:
            self._instance[pid] = self._decorated(*args, **kwargs)
            return self._instance[pid]

    def __call__(self):
        raise TypeError('One does not simply call a singleton')

class Singleton(object):
    def __init__(self, decorated):
        self._decorated = decorated

    def Instance(self, *args, **kwargs):
        try:
            return self._instance
        except AttributeError:
            self._instance = self._decorated(*args, **kwargs)
            return self._instance

    def __call__(self):
        raise TypeError('One does not simply call a singleton')

def get(url):
    """returns a get request from the provided url

    :param url: url
    :param filename: downloaded path and name to file
    :param verbose: print out progress
    :return: :class: Request object
    """
    try:
        return urllib.request.urlopen(url, timeout=30.0)
    except urllib.request.URLError as e:
        print(e.reason)
        print('Trying to fetch URL:', url)
        sys.exit(1)

def get_json(url):
    try:
        return json.load(get(url))
    except ValueError as e:
        raise(ValueError('While parsing document:\n\n{url}\n\npython2.7 -c "import urllib; print urllib.request.urlopen(\'{url}\').read()"'.format(url=url)))

def get_text(url):
    try:
        return get(url).read()
    except ValueError as e:
        raise(ValueError('While parsing document:\n\n{url}\n\npython2.7 -c "import urllib; print urllib.request.urlopen(\'{url}\').read()"'.format(url=url)))

def download(url, file_path=None, verbose=True):
    """downloads a get request from the provided url

    :param url: url
    :param file_path: downloaded path and name to file
    :param verbose: print out progress
    :return: :class: Request object
    """
    if verbose:
        print(url)
    if file_path:
        subprocess.call(['curl', '-o', file_path, url])
    else:
        subprocess.call(['curl', '-O', url])

def prompt(message, options=[]):
    if len(options) == 0:
        selection = None
        while selection != 'y' and selection != 'n':
            selection = input('{} [y/n]:'.format(message))
        return selection == 'y'
    else:
        print('\n'.join(['\t{:<4} ... {}'.format(str(i), d) for i,d in enumerate(selectable, 0)]))
        selection = int(input('Selection: '))
        if selection < 0 or selection >= len(selectable):
            raise ScriptError('selection out of bounds: ' + str(selection))
        return selection

def sift(regex, elements):
    return [e for e in elements if re.search(regex, e)]

def flatten(l):
    return flatten(l[0]) + (flatten(l[1:]) if len(l) > 1 else []) if type(l) is list else [l]

def except_map(e, f, t):
    def wrapper(i):
        try:
            return f(i)
        except e:
            return None
    return type(t)(filter(None, map(wrapper, t)))

def colored(text, color):
    color_codes = {
            'black': u'\u001b[30m',
            'red': u'\u001b[31m',
            'green': u'\u001b[32m',
            'yellow': u'\u001b[33m',
            'blue': u'\u001b[34m',
            'magenta': u'\u001b[35m',
            'cyan': u'\u001b[36m',
            'white': u'\u001b[37m',
            'reset': u'\u001b[0m',
            }
    return '{}{}{}'.format(color_codes[color], text, color_codes['reset'])

@Singleton
class RouteHandlers(dict):
    def __getattr__(self, item):
        return super().__getitem__(item)

    def __setattr__(self, item, value):
        return super().__setitem__(item, value)

def route(route):
    @functools.wraps(route)
    def handler(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            return f(*args, **kwargs)
        RouteHandlers.Instance()[route] = f
        return wrapper
    return handler

class HTTPHandler(http.server.BaseHTTPRequestHandler):
    def do_POST(self):
        content_length = self.headers['Content-Length']
        post_data = self.rfile.read(int(content_length)).decode('utf-8') if content_length else None
        handlers = RouteHandlers.Instance()
        if self.path in handlers:
            code = handlers[self.path](post_data)
            if code:
                self.send_response(code)
            else:
                self.send_response(200)
        else:
            self.send_response(404)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        handlers = RouteHandlers.Instance()
        if self.path in handlers:
            code = handlers[self.path]()
            if code:
                self.send_response(code)
            else:
                self.send_response(200)
        else:
            self.send_response(404)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

def run_server(port=8080):
    http.server.HTTPServer(('', port), HTTPHandler).serve_forever()

@Singleton
class ArgState(object):
    def __init__(self, state=None):
        self.state = state
        self.argv = sys.argv

    def is_commandline(self):
        return self.state != None

    def get_argv(self):
        return self.argv

    def __getattr__(self, attr):
        if self.state:
            return getattr(self.state, attr)

    def __getitem__(self, attr):
        if self.state:
            return getattr(self.state, attr)

@Singleton
class ArgHandlers(object):
    def __init__(self):
        self.handlers = {}

    def __getitem__(self, item):
        if item not in self.handlers:
            self.handlers[item] = []
        return self.handlers[item]

def argument_handler(f):
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        return f(*args, **kwargs)
    module_name = inspect.getmodule(inspect.currentframe().f_back).__name__
    ArgHandlers.Instance()[module_name].append(f)
    return wrapper

def run_argparse(description='', args=sys.argv[1:], prog=os.path.basename(sys.argv[0])):
    import argparse
    parser = argparse.ArgumentParser(prog=prog, description=description)
    parser.add_argument('-v', '--verbose', action='store_true')
    subparsers = parser.add_subparsers(dest='subcommand', title='subcommands', help='subcommands')
    subparsers.required = True

    if '--' in args:
        args = args[:sys.argv.index('--')]

    module_name = inspect.getmodule(inspect.currentframe().f_back).__name__
    for handler in ArgHandlers.Instance().handlers[module_name]:
        argument_name = handler.__name__
        argspec = inspect.getargspec(handler)
        handler.argspec = argspec
        if handler.__doc__:
            help_text = handler.__doc__.splitlines()[0]
        else:
            help_text = handler.__name__
    
        subparser = subparsers.add_parser(argument_name, help=help_text)
        subparser.set_defaults(handler=handler)
        required_args = argspec.args
        default_args = []

        if argspec.defaults:
            split_index = len(argspec.defaults)
            required_args = argspec.args[:-split_index]
            default_args = argspec.args[-split_index:]

        for arg in required_args:
            subparser.add_argument(arg)

        if argspec.defaults:
            first = len(argspec.defaults) == len(argspec.args)
            for arg, default in zip(default_args, argspec.defaults):
                arg_flag = arg if first else '--{}'.format(arg.replace('_', '-'))
                if type(default) is bool:
                    subparser.add_argument(arg_flag, default=default, action='store_true')
                elif type(default) is list:
                    subparser.add_argument(arg_flag, default=default, nargs='+')
                else:
                    def type_or_none(a):
                        return type(a) if a else None
                    nargs = '?' if first else None
                    subparser.add_argument(arg_flag, metavar=arg, type=type_or_none(default), default=default, nargs=nargs)
                first = False

    state = parser.parse_args(args)
    ArgState.Instance(state)

    call_args = {}
    for arg in state.handler.argspec.args:
        call_args[arg] = getattr(state, arg)
    result = state.handler(**call_args)
    if type(result) == int:
        sys.exit(result)
    if type(result) == subprocess.Popen:
        result.wait()
        sys.exit(result.returncode)
    elif result:
        pprint(result)
