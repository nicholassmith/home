if [ -z "$PS1" ]; then
  return
fi

current_git_branch() {
  git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || git symbolic-ref --short HEAD 2>/dev/null || git rev-parse --short HEAD 2>/dev/null
}

format_git_branch() {
  BRANCH=$(current_git_branch)
  if [ $BRANCH ]; then printf "%s " $BRANCH; else printf ""; fi
}

format_conda_env() {
  if [ $CONDA_DEFAULT_ENV ]; then printf "(%s) " $(basename $CONDA_DEFAULT_ENV); else printf ""; fi
}

vrep() {
  vim -q <(rg --vimgrep "$*" 2>/dev/null) -c ":cw"
}

gvrep() {
  vim -q <(git grep -n "$*" 2>/dev/null) -c ":cw"
}

frep() {
  find . -regex ".*$@"
}

gshow() {
  git log --graph --color=always --format="%C(auto)%h%d %s %C(black)%C(bold)%cr"  | \
  fzf --ansi --no-sort --reverse --tiebreak=index --preview \
    'f() { set -- $(echo -- "$@" | grep -o "[a-f0-9]\{7\}"); [ $# -eq 0 ] || git show --color=always $1 ; }; f {}' \
    --bind "j:down,k:up,alt-j:preview-down,alt-k:preview-up,ctrl-f:preview-page-down,ctrl-b:preview-page-up,q:abort,ctrl-m:execute:
        (grep -o '[a-f0-9]\{7\}' | head -1 |
        xargs -I % sh -c 'git show --color=always % | less -R') << 'FZF-EOF'
  {}
FZF-EOF" --preview-window=right:60%
}

kp() {
  _kp_PS=$(ps -ej | tail -n +2 | fzf)
  if [[ ! -z $_kp_PS ]]; then
    _kp_PID=$(echo $_kp_PS | awk '{ print $2 }')
    echo "$_kp_PS"
    read -p "Are you sure you want to kill [Y/n] " _kp_Yn
    if [[ $_kp_Yn != "n" ]]; then
      kill $@ $_kp_PID
      echo "Killed $_kp_PID"
    fi
  fi
}

sa() {
  if [[ "$#" -eq 0 ]]; then
    local screen_name=$(screen -list | grep "\t" | sed 's/[[:blank:]]*\(.*\)[[:blank:]]*(.*)/\1/g' | sed 's/[[:space:]]*$//' | \
      FZF_DEFAULT_OPTS="--height ${FZF_TMUX_HEIGHT:-40%} --reverse $FZF_DEFAULT_OPTS $FZF_COMPLETION_OPTS" fzf)
    screen -x "$screen_name"
  else
    local screen_name=$(echo "$*" | tr "/" "_")
    screen -x "$screen_name" 2>/dev/null || screen -S "$screen_name" $*
  fi
}

listcolors() {
  for i in {0..255}; do
    printf "%3d: \x1b[38;5;${i}mcolour${i}\x1b[0m\n" $i
  done
}

sdu() {
  df -h $*
  du -ch -d 1 $* 2>/dev/null | sort -hr
}

notify() {
  osascript -e 'display notification "'$@'" with title "Ping"'
}

preview() {
  fzf --preview 'head -n $(tput lines) {}'
}

todo() {
  screen -x todo || screen -S todo bash -c 'vim ~/src/todo/README.md'
}

irssi() {
  screen -x irssi || screen -S irssi bash -c 'irssi'
}

reset() {
if [ -n "$ZSH_VERSION" ]; then
  zle reset-prompt
  zle redisplay
  zle accept-line
elif [ -n "$BASH_VERSION" ]; then
  echo ""
fi
}

gcheckout() {
  git for-each-ref --sort=-committerdate --format='%(refname:short)' refs/heads/ | \
    FZF_DEFAULT_OPTS="--height ${FZF_TMUX_HEIGHT:-40%} --reverse $FZF_DEFAULT_OPTS $FZF_COMPLETION_OPTS" fzf | \
    xargs git checkout
  reset
}

wt() {
  local branch=$(git symbolic-ref --short HEAD)
  local session=$(basename $(pwd))
  tmux -L worktree -f ~/.tmux.worktree.conf new-session -e "PATH=$PATH" -e "EDITOR=$EDITOR" -A -s "$session" -n "$branch" $EDITOR
}

wt_remove() {
  local tmp=/tmp/wt_remove
  (git worktree list | awk '{ print "+ " $0; }') > $tmp
  $EDITOR $tmp
  local worktrees=$(grep "^- " $tmp | awk '{ print $2; }')
  while IFS= read -r worktree; do
    echo "git worktree remove $1 $worktree"
    git worktree remove $1 $worktree
  done <<< "$worktrees"
  git worktree prune
}

fs() {
  python3 -m http.server 8080 2>&1 > /dev/null &
}

fd() {
  local dir=$(tail -r ~/.zsh_dirhistory | sed 1d | \
    FZF_DEFAULT_OPTS="--height ${FZF_TMUX_HEIGHT:-40%} --reverse $FZF_DEFAULT_OPTS $FZF_COMPLETION_OPTS" fzf)
  if [ ! -z $dir ]; then
    cd $dir
  fi
  reset
}

if [ -n "$ZSH_VERSION" ]; then

  HISTSIZE=10000000
  SAVEHIST=10000000
  HISTFILE=~/.zsh_history
  setopt inc_append_history share_history hist_expire_dups_first hist_ignore_dups hist_ignore_all_dups hist_find_no_dups hist_ignore_space hist_save_no_dups hist_reduce_blanks hist_reduce_blanks
  bindkey -v
  bindkey -M viins 'kj' vi-cmd-mode
  autoload -U colors && colors
  setopt prompt_subst
  DIRSTACKSIZE=1024
  setopt auto_cd pushd_ignore_dups autopushd pushdminus pushdsilent pushdtohome
  alias dh='dirs -v'
  fpath=(~/.zsh $fpath)
  autoload -Uz compinit && compinit
  function precmd {
    export PROMPT="%(?..%{$fg[red]%}[$?] %{$reset_color%})%* %m %{$fg[blue]%}%1~ %{$fg_bold[green]%}$(format_git_branch)%{$reset_color%}$(format_conda_env)$ "
  }
  function chpwd {
    if [ -f ~/.zsh_dirhistory ]; then
      local line=$(grep -nxF "$PWD" ~/.zsh_dirhistory | cut -f1 -d:) || 
      echo $line
      if [ -z $line ]; then
        echo "$PWD" >> ~/.zsh_dirhistory
      else
        sed -i '' -e "$line{H;d;}" -e '${p;x;s/^\n//;}' ~/.zsh_dirhistory
      fi
    fi
  }
  zle -N gcheckout
  bindkey '^k' gcheckout
  zle -N sa
  bindkey '^a' sa
  zle -N fd
  bindkey '^j' fd
  [ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
  zstyle ':completion:*:*:git:*' script ~/.git-completion.bash

elif [ -n "$BASH_VERSION" ]; then

  # set this before fzf
  set -o vi
  bind '"kj":vi-movement-mode'
  bind -x '"\C-a":sa'
  [ -f ~/.git-completion.bash ] && source ~/.git-completion.bash
  source ~/.term_colors
  export PS1="\h \[$txtblu\]\W\[$bldgrn\] \$(format_git_branch)\[$txtrst\]\$ "
  [ -f ~/.fzf.bash ] && source ~/.fzf.bash
  export PROMPT_COMMAND="history -n; history -w; history -c; history -r; $PROMPT_COMMAND"
  shopt -s histappend
  export HISTCONTROL=ignoreboth:erasedups

fi

if [ -e ~/.nix-profile/etc/profile.d/nix.sh ]; then . ~/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
[ -f ~/.ghcup/env ] && source ~/.ghcup/env
[ -f ~/.localrc.sh ] && source ~/.localrc.sh

if [[ $SHLVL -eq 1 ]]; then
export PATH=$HOME/bin:$HOME/.local/bin:$HOME/miniconda3/bin:$HOME/.brew/bin:/opt/homebrew/bin:$PATH:$HOME/Library/Python/3.7/bin:$HOME/src/arcanist/bin:$HOME/.vim/plugged/fzf/bin
fi
export CLICOLOR=1
export EDITOR='nvim'
export LESS='-R'
export PAGER=less
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'
alias ls="ls --color"
alias l="ls -lah"
if [[ "$(uname -s)" == "Linux" ]]; then
alias open="xdg-open"
fi
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ......="cd ../../../../.."
alias .......="cd ../../../../../.."
alias ........="cd ../../../../../../.."
alias vim="nvim"
if [[ -f ~/.local/bin/tmux ]]; then
alias tmux="~/.local/bin/tmux"
fi
if [[ -x "$(command -v rg)" ]]; then
alias grep="rg"
fi
if [[ -f ~/.cargo/env ]]; then
. "$HOME/.cargo/env"
fi
