#!/bin/sh
set -x
set -e

# Setup base git repo
git init .
git remote add origin https://gitlab.com/nicholassmith/home.git
git fetch --tags --all
git reset --hard origin/home
git checkout home
git config --local status.showUntrackedFiles no

# Vanilla vim path
#vim -c "PlugInstall | qa"

# Install nvim
mkdir -p .local
case "$(uname -s)" in
  Linux*)     nvim_os=linux64;;
  Darwin*)    nvim_os=macos;;
  *)          echo "Unknown OS"; exit 1;;
esac
curl -LO https://github.com/neovim/neovim/releases/download/stable/nvim-$nvim_os.tar.gz
tar xvfz nvim-$nvim_os.tar.gz --strip-components=1 -C .local
rm -f nvim-$nvim_os.tar.gz

sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
         https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

~/.local/bin/nvim -c "PlugInstall | qa"

~/.config/nvim/plugged/fzf/install --key-bindings --no-completion --no-update-rc --no-fish
