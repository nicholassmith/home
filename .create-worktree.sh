EDITOR=~/.local/bin/nvim
session=$(basename $(pwd))
branch=$(git for-each-ref --sort=-committerdate --format='%(refname:short)' refs/heads/ | ~/.config/nvim/plugged/fzf/bin/fzf --reverse)
worktree=$(git worktree list | grep "\[$branch\]" | awk '{print $1;}')
win=$(tmux -L worktree list-windows -F "#I #W" | grep "$branch" | awk '{print $1;}')
if [ -z $win ]; then
  tmux -L worktree new-window -c $worktree -n "$branch" $EDITOR
else
  tmux -L worktree select-window -t "$win"
fi
