syntax on

filetype plugin indent on

let g:fzf_layout = { 'down': '40%' }

call plug#begin('~/.vim/plugged')
Plug 'edwinb/idris2-vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'psf/black', { 'branch': 'stable' }
Plug 'tikhomirov/vim-glsl'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
call plug#end()

runtime ftplugin/man.vim
set keywordprg=:Man
set ignorecase
set laststatus=2
set ruler
set number
set relativenumber
set shiftwidth=2
set tabstop=2
set softtabstop=2
set expandtab
set smartindent
set incsearch
set backspace=indent,eol,start
set scroll=20
set statusline=\ %{fnamemodify(getcwd(),':t')}\ [%{current_git_branch}]\ [%n]\ %f\ +%l\ %c\ %o\ %m%=%{current_file_time}\ %P\ %LL\ %y\ %r
set hlsearch
set cinoptions==0 " set case block indentation inline with case
set autoread " reload changed files on disk
set lazyredraw
set undofile " remember file undo history
set undodir=~/.vim/.undo//
set backupdir=~/.vim/.backup//
set directory=~/.vim/.swp//
set grepprg=rg\ --vimgrep
set splitbelow
"set switchbuf=vsplit
if exists('+termwinscroll')
set termwinscroll=100000
endif
set exrc
set secure
set tags=.git/tags

" Make diffs easier to read
if &diff
	set t_Co=16
	highlight DiffText cterm=reverse ctermbg=255 gui=bold
endif

function! s:FileTime()
	let g:current_file_time = strftime("%H:%M:%S %d-%b-%y", getftime(expand("%")))
	return g:current_file_time
endfunction
command! FileTime call s:FileTime()

function! s:CurrentGitBranch()
	let g:current_git_branch = system('(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || git symbolic-ref --short HEAD 2>/dev/null || git rev-parse --short HEAD 2>/dev/null) | tr -d "\n"')
	return g:current_git_branch
endfunction
command! CurrentGitBranch call s:CurrentGitBranch()

function! s:ZoomToggle() abort
    if exists('t:zoomed') && t:zoomed
        execute t:zoom_winrestcmd
        let t:zoomed = 0
    else
        let t:zoom_winrestcmd = winrestcmd()
        resize
        vertical resize
        let t:zoomed = 1
    endif
endfunction
command! ZoomToggle call s:ZoomToggle()

command! Ghistory Glog -- %

function! Formatonsave()
  let l:formatdiff = 1
  py3f ~/.clang-format.py
endfunction
autocmd BufWritePre *.h,*.hpp,*.c,*.cpp,*.cc call Formatonsave()

"if executable('darker')
"autocmd BufWritePost *.py silent :!darker %
"endif
"autocmd BufWritePre *.py execute ':Black'

autocmd BufEnter * CurrentGitBranch
autocmd BufEnter,BufRead,BufWrite * FileTime
autocmd FileType * nnoremap <silent> <leader><CR> :silent! bd make \| silent botright term make -f .vim.mk<CR>
autocmd FileType make setlocal noexpandtab
autocmd FileType /etc/sudoers.tmp setlocal noexpandtab
autocmd FileType javascript setlocal shiftwidth=2 softtabstop=2 tabstop=4 expandtab
autocmd FileType python setlocal shiftwidth=4 softtabstop=4 tabstop=8 expandtab 
autocmd FileType python nnoremap <silent> K :<C-u>execute 'silent !python3 -c "help(\"' . expand("<cword>") . '\")"'<CR><C-l>
autocmd FileType python vnoremap <silent> K y:<C-u>execute 'silent !python3 -c "help(' .  shellescape(@", 1) . ')"'<CR><C-l>
autocmd FileType python nnoremap <silent> <leader>i :silent! bd! !python3 \| botright term ++close python3 -i -c "import sys; sys.path.append('%:p:h'); import %:t:r; from %:t:r import *"<CR>
autocmd FileType idris2 nnoremap <silent> <leader><CR> :silent term ++close idris2 %<CR>
autocmd FileType haskell setlocal expandtab shiftwidth=2 softtabstop=2 tabstop=2
autocmd FileType haskell nnoremap <silent> K :silent !stack hoogle "<cword>" -- --info<CR>:redraw!<CR>
autocmd FileType haskell nnoremap <silent> <leader><CR> :silent term ++close stack ghci --ghci-options -XOverloadedStrings %<CR>
autocmd FileType cabal setlocal expandtab
autocmd Filetype yaml setlocal expandtab shiftwidth=2 softtabstop=2 tabstop=2
autocmd Filetype markdown setlocal expandtab shiftwidth=2 softtabstop=2 tabstop=2 foldmethod=indent nofoldenable
autocmd Filetype markdown nnoremap <silent> <leader>j :.s/\[\([x ]\)\]/\=submatch(1) == ' ' ? '[x]' : '[ ]'/g<CR>:noh<CR>:w<CR>
autocmd BufRead,BufNewFile *.mm setlocal filetype=objcpp
autocmd BufRead,BufNewFile *.nix setlocal shiftwidth=2 softtabstop=2 tabstop=2 expandtab filetype=nix
autocmd BufRead,BufNewFile *.tpp setlocal filetype=cpp
autocmd BufRead,BufNewFile *.inl setlocal filetype=cpp
autocmd BufRead,BufNewFile *.ini setlocal filetype=cpp
autocmd BufRead,BufNewFile *.metal setlocal filetype=cpp
autocmd BufRead,BufNewFile *.cl setlocal filetype=cpp
autocmd BufRead,BufNewFile *.hlsl setlocal filetype=cpp
autocmd BufRead,BufNewFile *.ll set filetype=llvm
autocmd BufRead,BufNewFile *.llvm set filetype=llvm
autocmd BufRead,BufNewFile *.td set filetype=tablegen
autocmd BufRead,BufNewFile *.fbs set filetype=fbs
autocmd BufRead,BufNewFile *.jsonpp set filetype=json
autocmd BufRead,BufNewFile *.gltf set filetype=json

command Hoogle execute "!stack hoogle % \| less"

let mapleader = " "
map <C-\> :vsp<CR>:exec("tjump ".expand("<cword>"))<CR>
map <C-]> :exec("tjump ".expand("<cword>"))<CR>
map <C-f> :py3f ~/.clang-format.py<cr>
imap kj <ESC>
cmap kj <C-c>
tmap kj <C-w>N
vnoremap v <ESC>
vnoremap <silent> # y/\V<C-R>=escape(@",'/\')<CR><CR>
nnoremap Y y$
nnoremap <leader>w :w<CR>
nnoremap <leader>Q :q!<CR>
nnoremap <leader>x :x<CR>
nnoremap <silent> <leader>z :ZoomToggle<CR>
nnoremap <silent> <leader>o :bprevious<CR>
nnoremap <silent> <leader>i :bnext<CR>
nnoremap <leader>k :Ggrep <C-R><C-W><CR><CR>:cw<CR>
nnoremap <silent><leader>/ :BLines<CR>
nnoremap <leader>? :Rg 
nnoremap <leader>r :source $MYVIMRC<CR>
"nnoremap <leader>e :bufdo e<CR>
nnoremap <leader>e :e<CR>
nnoremap <leader>E :e!<CR>
nnoremap <leader>l :set invrelativenumber<CR>
nnoremap <leader>y "+y
nnoremap <leader>p "+p
nnoremap <leader>P "+P
nnoremap <silent> <C-p> :GFiles<CR>
nnoremap <silent> <C-h> :History<CR>
nnoremap <silent> <C-j> :Buffers<CR>
nnoremap <silent> <C-k> :Tags <C-R><C-W><CR>
nnoremap <silent><leader>t :silent! sb !/bin/zsh \| silent botright terminal<CR>
nnoremap <silent><leader>f ofprintf(stderr, "%s:%d\n", __func__, __LINE__);<ESC>
nnoremap <leader>c o_(_ const&) = delete;<CR>_(_ const&&) = delete;<CR>_& operator=(_ const&) = delete;<CR>_& operator=(_ const&&) = delete;<ESC>V3k:s/_/
vnoremap <leader>v :s/typedef struct \(Vk\(\w\+\)\)/\1 \l\2 =/<CR>gv:s/} \w\+;/};/<CR>gv:s/.\+ \(\w\+\);/\.\1 = 0,/<CR>
"nnoremap <silent><C-h> :vertical resize -10<CR>
"nnoremap <silent><C-l> :vertical resize +10<CR>
"nnoremap <silent><C-j> :resize -10<CR>
"nnoremap <silent><C-k> :resize +10<CR>
nnoremap <silent><leader>i :silent term ++close ++rows=16 python3<CR>

" auto-brackets
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O
