set -g prefix C-b
bind-key C-b send-prefix

# Prev window
bind-key C-b last-window
bind -n M-Space last-window
# Nested Tmux
bind-key a send-prefix
bind-key -n C-a send-prefix

# Start at 1
set -g base-index 1
setw -g pane-base-index 1

set-option -g default-shell /bin/zsh

# Vi Mode
set-window-option -g mode-keys vi
#bind-key -t vi-copy 'v' begin-selection
#bind-key -t vi-copy 'y' copy-selection
#unbind -t vi-copy MouseDragEnd1Pane

# Copy-paste
bind-key -T copy-mode-vi 'v' send -X begin-selection
bind-key -T copy-mode-vi 'y' send -X copy-pipe-and-cancel pbcopy
bind-key -T copy-mode-vi MouseDragEnd1Pane send -X copy-pipe-and-cancel pbcopy
bind-key -T copy-mode-vi MouseDragEnd3Pane send -X copy-pipe-and-cancel pbcopy

# Splits
bind-key v split-window -h
bind-key s split-window -v
bind-key -n M-v split-window -h
bind-key -n M-s split-window -v

bind -n M-H resize-pane -L 5
bind -n M-J resize-pane -D 5
bind -n M-K resize-pane -U 5
bind -n M-L resize-pane -R 5

bind -n M-z resize-pane -Z

# hjkl pane traversal
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R
bind -n M-h select-pane -L
bind -n M-j select-pane -D 
bind -n M-k select-pane -U
bind -n M-l select-pane -R

# Window create/kill/traversal
bind -n M-Enter new-window
bind -n M-q confirm-before -p "kill-window #W? (y/n)" kill-window
bind -n M-0 select-window -t 0
bind -n M-1 select-window -t 1
bind -n M-2 select-window -t 2
bind -n M-3 select-window -t 3
bind -n M-4 select-window -t 4
bind -n M-5 select-window -t 5
bind -n M-6 select-window -t 6
bind -n M-7 select-window -t 7
bind -n M-8 select-window -t 8
bind -n M-9 select-window -t 9

bind -n M-) swap-window -t 0
bind -n M-! swap-window -t 1
bind -n M-@ swap-window -t 2
bind -n M-'#' swap-window -t 3
bind -n M-'$' swap-window -t 4
bind -n M-% swap-window -t 5
bind -n M-^ swap-window -t 6
bind -n M-& swap-window -t 7
bind -n M-* swap-window -t 8
bind -n M-( swap-window -t 9

bind -n M-r next-layout
bind -n M-R previous-layout

bind -n M-g source-file ~/.tmux.conf \; display-message "Reloaded..."

# Enable mouse
#setw -g mode-mouse on
#set-option -g mouse on

# Search
#bind-key -t copy-mode 'e' search-backward error:

# Theme
BG="default"
FG="black"
%if #{==:#{host_short},protato}
THEME="cyan"
%elif #{==:#{host_short},monad}
THEME="green"
%elif #{==:#{host_short},crouton}
THEME="white"
%else
THEME="yellow"
%endif
set -g pane-border-style "bg=$BG,fg=$THEME"
set -g pane-active-border-style "bg=$BG,fg=$THEME"
set -g window-status-current-style "bg=$BG,fg=$FG,bright"
set -g window-status-style "bg=$BG,fg=$FG"
set -g status-style "bg=$THEME,fg=$FG"
set -g status-justify centre

set -g window-status-format "#I:#W"
set -g window-status-current-format "#I*#W"
set -g automatic-rename-format "#{b:pane_current_path}"
set -g automatic-rename-format "#{b:pane_current_path}"

set -g history-limit 10000
